import { IUser } from './State/GameplayState';
import {state, syncPiece} from './../gameplayHandler';
import { collisionTags, collisionGroups } from './../collisionTag';
import {convertToThreeVec3, createRigidBodyForGroup, uuidv4} from './../utils';
import * as THREE from "three";
import * as CANNON from "cannon-es";
import { cannonTypeMaterials, CyclinderBasicParam } from "../main";
import GameObject from "./GameObject";
import { timers } from 'jquery';
import {SyncPieceStatePosition} from "../gameEvent";

const _dbg = (params) => {
  console.log(params)
}

// 0.08, 0.7, 2, 50
// assume 4 con số trên là của cyclinder 

export default class Piece extends GameObject {
  args: CyclinderBasicParam;
  active: boolean;
  color: string;

  userId: string;

  initPosition: CANNON.Vec3; // position x, y, z
  targetPoint: CANNON.Vec3;
  isTouchBoard: boolean;
  prevGoal: number;
  nextStep: number;
  goal: number;
  isReturn: boolean;
  atBase: boolean;
  order: number;

  currentPosStatus: string; // common | final | base ( currentPosIndex = -1 )
  currentPosIndex: number; // commonPath index | finalPath index

  // isStartModeAuto; // no need, when nextStep > 0 piece jump

  private hack_finishCheat: boolean = true;
  public hack_maxJump: boolean = false;

  constructor(color: string, order: number, args: CyclinderBasicParam, position: number[], world: CANNON.World, userId: string) {
    super();

    this.world = world;
    this.args = args;
    this.position = new CANNON.Vec3(...position);
    this.initPosition = new CANNON.Vec3(...position);
    this.targetPoint = this.initPosition;
    this.currentPosStatus = 'base';
    this.currentPosIndex = -1;
    this.isTouchBoard = true;
    this.prevGoal = 0;
    this.nextStep = -1;
    this.goal = -1;
    this.isReturn = false;
    this.atBase = true;
    this.color = color;
    this.order = order;
    this.mass = 50500;

    this.userId = userId;

    // this.isStartModeAuto = false;
  }

  checkAvailable = (step: number) => {
    if (this.atBase) {
      if (state.getGamePiece(this.userId).filter(x => x.currentPosIndex == 0).length > 0)
        return false;
      // if (step % 2 != 0)
      //   return false;
    } else {
      const allOtherUser: IUser[] = state.getListUserInRoom().filter(x => x.id != this.userId);
      const allCommonPiece: Piece[] = [];

      const testOtherPiece: {equivalentIndex: number}[] = [];
      for (const user of allOtherUser) {
        allCommonPiece.push(...state.getGamePiece(user.id)
          .filter(x => !x.atBase && x.currentPosStatus == "common"));
      }
      for (const piece of allCommonPiece) {
        const otherUser = state.getListUserInRoom().find(x => x.id == piece.userId);
        const thisUser = state.getListUserInRoom().find(x => x.id == this.userId);
        const curIndex = piece.currentPosIndex;
        const curSection = Math.floor(curIndex / 13) + 1;
        const orderDistance = Math.abs(otherUser.order - thisUser.order);

        const equivalentSection = 
          otherUser.order < thisUser.order 
            ? (curSection - orderDistance <= 0)
              ? curSection - orderDistance + 4
              : curSection - orderDistance
            : (curSection + orderDistance) % 4 == 0 
              ? 4 
              : (curSection + orderDistance) % 4;

        // const equivalentSection = (curSection - orderDistance <= 0)
        //   ? curSection - orderDistance + 4
        //   : curSection - orderDistance;

        testOtherPiece.push({
          equivalentIndex: (curIndex % 13) + ((equivalentSection - 1) * 13),
        })
      }
      if (
        testOtherPiece
          .filter(x => x.equivalentIndex > this.currentPosIndex 
            && x.equivalentIndex < this.currentPosIndex + step).length > 0
      ) {
        return false;
      }

      const frontPiece = state
        .getGamePiece(this.userId)
        .filter(x => x.order != this.order && !x.atBase && x.currentPosIndex > this.currentPosIndex);

      if (frontPiece.length > 0) {
        if (frontPiece.filter(x => x.currentPosIndex <= this.currentPosIndex + step).length > 0) {
          return false
        }
      } else {
        // 57 = common path + final path
        if (this.currentPosIndex + step >= 57) {
          return false;
        }
      }


    }
    return true;
  }

  isAtFinished = () => {
    let NUM_RECT = 52; // 52 = number of rectangle on a piece board
    if (this.currentPosIndex >= NUM_RECT) {
      return true;
    }
    return false;
  }

  // targetType: common | final | base
  jumpToIdx = (targetPoint, targetIdx, targetType) => {
    if (targetType == "base") {
      this.targetPoint = new CANNON.Vec3(...<number[]>Object.values(targetPoint));
      this.atBase = true;
      this.currentPosStatus = 'base'
      this.currentPosIndex = targetIdx;
      this.prevGoal = 0;
      this.nextStep = -1;
      this.goal = -1;

      this.launch(new CANNON.Vec3(0, 30, 0));
      // piece.returnBase();
    } else if (targetType == "final") {
      const commonLength = 52;
      this.targetPoint = new CANNON.Vec3(...<number[]>Object.values(targetPoint));
      this.launch(new CANNON.Vec3(0, 40, 0));

      this.currentPosStatus = 'final';
      this.currentPosIndex = commonLength + targetIdx;
      this.prevGoal = this.currentPosIndex + 1;
      this.nextStep = -1;
      this.atBase = false;
    } else {
      this.targetPoint = new CANNON.Vec3(...<number[]>Object.values(targetPoint));
      this.launch(new CANNON.Vec3(0, 40, 0));
      this.currentPosStatus = 'common';
      this.currentPosIndex = targetIdx;
      this.prevGoal = this.currentPosIndex + 1;
      this.nextStep = -1;
      this.atBase = false;
    }
  }

  updateVictoryState = () => {
    const commonLength = 52;
    if (this.isAtFinished()) {
      let victoryState = state.getVictoryState();
      let pieces = victoryState.find(x => x.userId == this.userId).pieces;
      let piece = pieces.find(e => e[0] == this.order);
      if (piece) {
        piece[1] = this.currentPosIndex - commonLength;
      } else {
        pieces.push([this.order, this.currentPosIndex - commonLength]);
      }
    }
  }

  initObject = async () => {
    const listMesh: THREE.Mesh[] = [];

    const baseGeometry = new THREE.SphereBufferGeometry(0.5, 32, 32);
    const baseMaterial = new THREE.MeshPhysicalMaterial({
      reflectivity: 1.0, clearcoat: 1.0,
      color: this.active ? "purple" : this.color,
    });

    listMesh.push(new THREE.Mesh(baseGeometry, baseMaterial));
    listMesh[0].position.add(new THREE.Vector3(0, 1, 0)); // sphere on top
    listMesh[0].receiveShadow = false;
    listMesh[0].castShadow = false;

    const topGeometry = new THREE.CylinderBufferGeometry(
      ...Object.values(this.args)
    );
    listMesh.push(new THREE.Mesh(topGeometry, baseMaterial));
    listMesh[1].receiveShadow = false;
    listMesh[1].castShadow = false;

    this.addMesh(...listMesh);

    this.initRigidBody({}, cannonTypeMaterials['ground']);
    // this.rigidBody.collisionFilterGroup = collisionGroups.piece;
    // this.rigidBody.collisionFilterMask = collisionGroups.board;

    this.rigidBody['tag'] = collisionTags.piece;
    this.rigidBody['userId'] = this.userId;
    this.rigidBody['order'] = this.order;

    this.rigidBody.addEventListener('collide', ev => {
      if (ev.body.tag === collisionTags.board) {
        this.isTouchBoard = true;
      } else {
        this.isTouchBoard = false;
      }

      if (this.nextStep >= 0) {
        if (ev.body.tag == collisionTags.piece) {

          const piece = state.getGamePiece(ev.body.userId).find(x => x.order == ev.body.order);
          if (piece.userId != this.userId && !piece.isAtFinished()) {
            piece.returnBase();
          }
        }
      }
    });

    this.mainModel["objInfo"] = {
      tag: 'piece',
      userId: this.userId,
      order: this.order,
      body: this.rigidBody,
      raycast: true,
    }
  }

  isOnGround = () => {
    return Math.abs(this.rigidBody.velocity.y) <= 0.05;
  }

  handleAutoJumpMode = (commonPath: any[], finalPath: any[]) => {
    if (!commonPath || commonPath.length < 52)
      return;

    if (this.nextStep >= 0) {
      if (this.isOnGround()) {
        if (this.nextStep + this.prevGoal >= this.goal) {
          this.nextStep = -1;
          this.prevGoal = this.goal;
        } else {
          if (this.nextStep + this.prevGoal >= commonPath.length) {
            this.currentPosStatus = 'final';
            this.currentPosIndex = this.prevGoal + this.nextStep;

            this.targetPoint = new CANNON.Vec3(
              ...<number[]>Object.values(finalPath[((this.nextStep++) + this.prevGoal) - commonPath.length]));
          }
          else {
            this.currentPosStatus = 'common';
            this.currentPosIndex = this.prevGoal + this.nextStep;

            this.targetPoint = new CANNON.Vec3(
              ...<number[]>Object.values(commonPath[(this.nextStep++) + this.prevGoal]));
          }

          if (this.atBase) {
            this.launch(new CANNON.Vec3(0, 40, 0));
            this.atBase = false;
          } else { this.launch(new CANNON.Vec3(0, 25, 0)); }
        }
      }
    }
  }

  handleMapPoint = () => {
    if (!this.targetPoint)
      return;
    let targetVeloc = this.targetPoint.vsub(this.rigidBody.position);
    targetVeloc = targetVeloc.scale(25);

    this.rigidBody.velocity.x = targetVeloc.x;
    this.rigidBody.velocity.z = targetVeloc.z;
    this.setRotation(new CANNON.Vec3(0, 0, 0));
    // this.rigidBody.velocity.set(targetVeloc.x, targetVeloc.y, targetVeloc.z)
  }

  // goByStep = (userId: string, step: number, order: number) => {
  //   if (userId === state.getUserId() && order === this.order) {
  //     this.nextStep++;
  //     this.goal = this.prevStep + step;
  //   }
  // }

  colorAvailable = () => {
    for (const mesh of <THREE.Mesh[]>this.mainModel.children) {
      mesh.material["color"].set("#E400CF");
    }
  }

  colorUnAvailable = () => {
    for (const mesh of <THREE.Mesh[]>this.mainModel.children) {
      mesh.material["color"].set("#808B96");
    }
  }

  colorDefault = () => {
    for (const mesh of <THREE.Mesh[]>this.mainModel.children) {
      mesh.material["color"].set(this.color)
    }
  }


  returnBase = () => {
    // this.targetPoint = this.initPosition;
    // this.atBase = true;
    // this.prevGoal = 0;
    // this.currentPosStatus = 'base'
    // this.currentPosIndex = -1;
    // this.nextStep = -1;
    // this.goal = -1;
    //
    // this.launch(new CANNON.Vec3(0, 30, 0));
    this.jumpToIdx(this.initPosition.toArray(), -1, "base");
  }


  goByStep = (step: number) => {
    const commonPath = state.getUserCommonPath(this.userId);
    const finalPath = state.getUserFinalPath(this.userId);

    if (this.prevGoal + step > commonPath.length + finalPath.length)
      return;
    this.nextStep++;
    this.goal = this.prevGoal + step;
  }

  keyboardHandle = (table) => {
    this.rigidBody.wakeUp(); // very important


    let keycode = require('keycode');

    if (table[keycode('l')]) {
      if (this.currentPosStatus == 'common')
        _dbg([this.currentPosIndex, this.goal, this.prevGoal]);
      // _dbg([this.currentPosIndex, this.goal, this.prevGoal])
    }
    // if (table[keycode('q')]) {
    //   this.goByStep(state.getUserId(), 2, 1);
    // }
  }

  update = () => {
    // update rigidBody upon value from object
    const commonPath = state.getUserCommonPath(this.userId);
    const finalPath = state.getUserFinalPath(this.userId);

    this.handleAutoJumpMode(commonPath, finalPath);
    this.handleMapPoint();
    this.updateVictoryState();

    this.mainModel.position.fromArray(Object.values(this.rigidBody.position));
    this.mainModel.quaternion.fromArray(Object.values(this.rigidBody.quaternion));

    this.setRotation(new CANNON.Vec3(0, 0, 0));
  }

  getMesh = async () => {
    await this.initObject();

    return this.mainModel;
  }
}
