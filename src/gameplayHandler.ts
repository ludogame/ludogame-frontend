import GameplayState, {IPiece, IRoomClient, IUser, IVictory, IRankingUI} from './components/State/GameplayState';
import $ from 'jquery';
import { getFormSubmitValue, uuidv4 } from "./utils";
import {
  GetUserReady,
  MeJoin,
  StartGame,
  StartTurn,
  SyncPieceState,
  SyncPieceStatePosition,
  ThrowDice, UserEndGame,
  UserJoin,
  UserLeave,
  UserReady,
  UserSkipTurn
} from "./gameEvent";
import Piece from './components/Piece';
import DiceCanvas from './components/DiceCanvas';
import { chatState, createChatRoom, showChatBox, joinChatRoom } from './chatHandler';

// require('./chatHandler'); // load gameplay chat

export const state = new GameplayState(window.sessionStorage.getItem('userId'));
export const diceManager = new DiceCanvas($('.gameToolBox .diceArea .diceAreaPlayground')[0]);

window.onload = (ev) => {
  url_handle(false);
  chatState.configChatState(state.getClient(), state.getUserId());
  diceManager.initWorker();

  getRoom();
  configToolBoxOnState();
}

export const toggleSelectRoom = (signal = true) => {
  flipDisplay($('.selectRoom'), signal);
}

export const toggleGameRoom = (signal: boolean  = true) => {
  flipDisplay($('.pendingScreen'), !signal);
  flipDisplay($('.mainScreen'), signal);
  toggleSelectRoom(signal);
}

// URL handle
const url_handle = (signout = false) => {
  if (window.location.pathname == '/') {
    if (signout == true) {
      window.sessionStorage.clear();
      window.location.href = '/account';
      toggleGameRoom(false);
    } else {
      if (!window.sessionStorage.getItem('userId'))
        window.location.href = '/account'
      else toggleGameRoom(true);
    }
  }
}

const loadGame = () => {
  // state.getGameRoom().onMessage(ThrowDice, mess => {
  //   diceManager.throwDiceOnSchema(mess.dice, mess.camera);
  // });


  $('.gameToolBox .toolBox #throwDice').on('click', ev => {
    if (state.getHaveThrowDiceStatus() || !diceManager.nextThrowReady)
      return;

    state.getGameRoom().send(ThrowDice, {
      userId: state.getUserId(),
    });
    state.setPointDice1(0);
    state.setHaveThrowDiceStatus(true);
    state.setSkipTurnStatus(false);

    configToolBoxOnState();
  });

  $('.gameToolBox .toolBox #skipTurn').on('click', ev => {
    if (!state.getSkipTurnStatus())
      return;

    state.setSkipInterval(0, null);
    state.resetToolboxState();
    state.setCurrentTurn('');
    state.setPointDice1(0);
    // state.setPointDice2(0);
    state.getGameRoom().send(UserSkipTurn, {userId: state.getUserId()});
    configToolBoxOnState();
  });
  $('.gameToolBox .toolBox #movePiece').on('click', ev => {
    if (!state.getCanMovePieceStatus())
      return

    alert('Please click directly on your dice');
  });
};

const displayDots = (num: number, jqueryComponent) => {
  let cls = 'odd-'
  if (num % 2 === 0) {
    cls = 'even-'
  }

  $(jqueryComponent).empty();
  for (let i = 1; i <= num; i++) {
    $(jqueryComponent).append('<div class="dot ' + cls + i + '"></div>');
  }
}

var hack_finishCheat = true;
const hack_finishAllPiece = (finalPaths, listPieces: Piece[]) => {
  const commonLength = 52;
  let finalPaths_t = [...finalPaths].map((v, i) => [v, i])

  let listPieceIdx = [];
  for (const piece of listPieces) {
    let finishPos = piece.currentPosIndex - commonLength;
    if (finishPos >= 0) {
      finalPaths_t = finalPaths_t.filter((v) => v[1] != finishPos);
    } else {
      listPieceIdx.push(piece.order);
    }
  }
  // _dbg({"[FinalPath]": finalPaths_t});
  // _dbg({"[ListIDX] ": listPieceIdx});

  const shuffleArray = (array: any[]) => {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }
  shuffleArray(finalPaths_t);

  let listPieceIdx_slice = listPieceIdx.slice(0, listPieceIdx.length);
  if (Math.random() > 0.2) {
    listPieceIdx_slice = listPieceIdx.slice(0, Math.floor(Math.random() * listPieceIdx.length))
  }

  listPieceIdx_slice.forEach(v => {
    let pieceOrder = v;
    let path = finalPaths_t.pop();
    state.getGameRoom().send(SyncPieceStatePosition, {
      "userId": state.getUserId(),
      "pieceOrder": pieceOrder,
      "targetPoint": path[0],
      "targetIdx": path[1],
      "targetType":  "final"
    })
  });

  setTimeout(() => {hack_finishCheat = true},500);
  // this.hack_finishCheat = true;
}

const handleRankingUI = (rankingState: IRankingUI[]) => {
  const UIArr = [
    [".leader_board .three_people .vic_1st", ".leader_board .list_user .list_usr_01"],
    [".leader_board .three_people .vic_2nd", ".leader_board .list_user .list_usr_02"],
    [".leader_board .three_people .vic_3rd", ".leader_board .list_user .list_usr_03"],
    [".leader_board .list_user .list_usr_04"],
  ];

  let idx = 0;

  for (let rstate of rankingState) {
    if (idx >= 4) {break;}

    for (let UIselect of UIArr[idx]) {
      let usr_icon = $(UIselect + " .sta_userIcon img");
      let usr_name = $(UIselect + " .sta_name");
      let usr_finish = $(UIselect + " .sta_state .sta_finished");
      let usr_notFinish = $(UIselect + " .sta_state .sta_notFinished");

      usr_icon.prop("src", rstate.avatar);
      usr_name.text(rstate.name);

      if (rstate.status == 1) {
        usr_finish.css("display", "block");
        usr_notFinish.css("display", "none");
      } else {
        usr_finish.css("display", "none");
        usr_notFinish.css("display", "block");
      }
    }
    idx += 1;
  }
}


const calRanking = (victoryState: IVictory[]) : IRankingUI[] => {
  let res: IRankingUI[] = [];
  const rankPosAvailable = 4;

  let userQuit: IVictory[] = [];
  let userFinished: IVictory[] = [];

  for (let state of victoryState) {
    if (state.isQuit == true || state.pieces.length <= 0) {
      userQuit.push(state);
    } else {
      userFinished.push(state);
    }
  }

  const sumPieceRank = (pieces: [number, number][]): number => {
    let res = 0;
    pieces.forEach(v => res += v[1]);
    return res;
  }

  userFinished.sort((a, b) =>
      Math.pow(sumPieceRank(b.pieces), b.pieces.length) -
      Math.pow(sumPieceRank(a.pieces), a.pieces.length));

  userFinished.forEach((x, i) => {
    let userInfo = state.getListUserInRoom().find(a => a.id == x.userId);
    if (res.length < rankPosAvailable) {
      res.push({
        id: x.userId,
        rank: i + 1,
        status: 1,
        ...userInfo
      });
    }
  });

  userQuit.forEach((x, i) => {
    if (res.length < rankPosAvailable) {
      let userInfo = state.getListUserInRoom().find(a => a.id == x.userId);
      res.push({
        id: x.userId,
        rank: -1,
        status: 0,
        ...userInfo
      });
    }
  });

  // userNotDefined
  const USER_NOT_FOUND_ICO: string = "resource/imgs/user_not_found.png";
  Array(rankPosAvailable - res.length).fill(1).forEach(x => {
    if (res.length < rankPosAvailable) {
      res.push({
        id: "",
        rank: -1,
        status: -1,
        address: "",
        avatar: USER_NOT_FOUND_ICO,
        order: -1,
        name: "Unknown...",
        jobTitle: ""
      });
    }
  });
  return res;
}

const flipDisplay = (jqueryComp: JQuery<HTMLElement>, signal: boolean, displayState = "flex") => {
  if (!signal) {
    if (jqueryComp.css("display") != "none") {
      jqueryComp.css("display", "none");
    }
  } else {
    if (jqueryComp.css("display") == "none") {
      jqueryComp.css("display", displayState);
    }
  }
}

const toggleLeaderboard = (signal: boolean = true) => {
  flipDisplay($(".pendingScreen"), !signal); // off
  flipDisplay($(".mainScreen"), !signal); // off
  flipDisplay($(".leader_board_region"), signal); // on

  if (signal) {
    handleRankingUI(calRanking(state.getVictoryState()));
    $(".leader_board_region").hide(0);
    $(".leader_board_region").show(500);
  }
}

document.addEventListener("keydown", ev => {
  if (ev.key == "f") {
    if (hack_finishCheat && state.getGameStarted() == true) {
      hack_finishCheat = false;
      hack_finishAllPiece(state.getUserFinalPath(state.getUserId()), state.getGamePiece(state.getUserId()));
      // _dbg("[CHEAT] Cheat on" + uuidv4());
    }
  } else if (ev.key == "d") {
    let res = calRanking(state.getVictoryState());
    _dbg('[DEBUG] calranking');
    _dbg(res);
    _dbg("<<<");
  }
});

// document.addEventListener("keydown", ev => {
//   if (ev.key == 'w') {
//     const piece = state.getGamePiece(state.getUserId())[0];
//     piece.goByStep(5);
//   }
//   if (ev.key == 'r') {
//     const piece = state.getGamePiece(state.getUserId())[0];
//     piece.returnBase();
//   }
//   if (ev.key == 's') {
//     const piece = state.getGamePiece(state.getUserId())[0];
//     piece.goByStep(1);
//   }
// })

const configUserAction = () => {
  if (state.getCurrentTurn() == state.getUserId()) {
    if (state.getHaveThrowDiceStatus() && state.getPointDice1() != 0) {
      // const pieces = state.getGamePiece(state.getUserId());

      state.setCanMovePieceStatus(true)
      // if (pieces.filter(x => x.checkAvailable(state.getPointDice1())).length > 0) {
      //   state.setCanMovePieceStatus(true);
      // } else state.setCanMovePieceStatus(false);
    } else state.setCanMovePieceStatus(false);
  } else {
    state.resetToolboxState();
  }
}

export const configToolBoxOnState = () => {
  const dice1 = state.getPointDice1();
  // const dice2 = state.getPointDice2();

  displayDots(dice1, '#dice1');
  // displayDots(dice2, '#dice2');

  $('.gameToolBox .toolBox #throwDice').prop('disabled', true);
  $('.gameToolBox .toolBox #movePiece').prop('disabled', true);
  $('.gameToolBox .toolBox #skipTurn').prop('disabled', true);

  configUserAction(); // change state

  const displayBasedOnState = () => {
    if (!state.getHaveThrowDiceStatus()) {
      $('.gameToolBox .toolBox #throwDice').prop('disabled', false);
    } else $('.gameToolBox .toolBox #throwDice').prop('disabled', true);

    if (state.getCanMovePieceStatus())
      $('.gameToolBox .toolBox #movePiece').prop('disabled', false);
    else $('.gameToolBox .toolBox #movePiece').prop('disabled', true);

    if (state.getSkipTurnStatus())
      $('.gameToolBox .toolBox #skipTurn').prop('disabled', false);
    else $('.gameToolBox .toolBox #skipTurn').prop('disabled', true);
  }
  displayBasedOnState();
}

const setUserStatus = (val: IUser) => {
  const elementSelectString = `#${val.id} .user-list-favourite-time`;
  $(elementSelectString).empty();

  const user = <IUser>state.searchUserInRoom(val.id);
  user.isReady = val.isReady;

  if (user.isReady) {
    $(elementSelectString).append(
      $(`
        <a class="user-list-favourite order-2 text-success" href="#"><i class="fas fa-  check-circle"></i></a>
        <span class="user-list-time order-1">Ready</span>
      `));
  } else {
    $(elementSelectString).append(
      $(`
        <a class="user-list-favourite order-2 text-info" href="#"><i class="fas fa-clock"></i></a>
        <span class="user-list-time order-1">Waiting...</span>
      `));
  }
}

const setUserTurnIcon = (userId: string) =>  {
  const elementSelectString = `#${userId}`;

  $('.userInRoom .userInRoomList .users-list').removeClass('users-main');
  $(elementSelectString).addClass('users-main');
}

const handleShowUserInfo = (ev: JQuery.ClickEvent, id: string, isModal: boolean = true) => {

}

const handleShowChatBox = (ev: JQuery.ClickEvent, id: string) => {

}

const handleUserReady = (mess: any) => {
  if (mess.user.id === state.getUserId())
    state.getGameplay().setCameraStopOrbitAuto(mess.camera);

  state.setUserCommonPath(mess.user.id, mess.commonPath.data);
  state.setUserFinalPath(mess.user.id, mess.finalPath.data);
  state.setUserPiece(mess.user.id, <IPiece[]>mess.pieces.data);
  state.getVictoryState().push({
    userId: mess.user.id,
    pieces: [],
    isQuit: false
  });

  // load piece to map
  state.getGameplay().addObject(
    state.getUserPiece(mess.user.id).map(x => {
      const piece = new Piece(
        x.color, x.order,
        {
          radiusTop: 0.08,
          radiusBottom: 0.7,
          radialSegments: 2,
          heightSegments: 50
        },
        Object.values(x.initPosition),
        state.getGameplay().getWorld(),
        mess.user.id,
      );
      state.addGamePiece(mess.user.id, piece);
      return piece;
    })
  )
}

const handleUpdatePiece = (mess: any) => {
  const piece = <Piece>state.getGamePiece(mess.userId).find(x => x.order === mess.data.order);
  piece.targetPoint = mess.data.targetPoint;
  piece.prevGoal = mess.data.prevStep;
  piece.nextStep = mess.data.nextStep;
  piece.goal = mess.data.goal;
  piece.isReturn = mess.data.isReturn;
  piece.atBase = mess.data.atBase;
}

export const syncPiece = (piece: Piece) => {
  // sync data

  // step: 1,
  // userId: listPieceAvailable[0].userId,
  // order: listPieceAvailable[0].order,

  let step = state.getPointDice1();
  if (piece.atBase) step = 1;

  state.setCurrentTurn('');
  state.setCanMovePieceStatus(false);
  state.setPointDice1(0);
  // state.setPointDice2(0);

  state.getGameRoom().send(SyncPieceState, {
    step, userId: piece.userId, order: piece.order,
  });
  state.getGameRoom().send(UserSkipTurn, {userId: state.getUserId()});

  configToolBoxOnState();
}

const handleAddUserUI = (mess: IUser) => {
  // state.addUserInRoom(mess);

  const element = $(`
    <tr class="users-list" id="${mess.id}">
      <td class="title">
        <div class="thumb">
          <img class="img-fluid" src="${mess.avatar}" alt="">
        </div>
        <div class="user-list-details">
          <div class="user-list-info">
            <div class="user-list-title">
              <h5 class="mb-0"><a class="showUserInfo" href="#">${mess.name} 
                ${mess.id === state.getUserId() ? "( YOU )" : ""}</a></h5>
            </div>
            <div class="user-list-option">
              <ul class="list-unstyled">
                <li><i class="fas fa-filter pr-1"></i>${mess.jobTitle}</li>
                <li><i class="fas fa-map-marker-alt pr-1"></i>${mess.address}</li>
              </ul>
            </div>
          </div>
        </div>
      </td>
      <td class="user-list-favourite-time text-center">
        
      </td>
      <td>
        <ul class="list-unstyled mb-0 d-flex justify-content-end">
          <li><a class="showUserInfo" class="text-primary" data-toggle="tooltip" title="" data-original-title="chat"><i class="far fa-eye"></i></a></li>
          <li><a class="showChatBox class="text-info" data-toggle="tooltip" title="" data-original-title="view"><i class="far fa-comment-dots"></i></a></li>
        </ul>
      </td>
    </tr>
  `)
  $('.userInRoom tbody').append(element);

  $(`.userInRoom tbody #${mess.id} .list-unstyled .showUserInfo`).on('click', ev => {
    ev.preventDefault();
    handleShowUserInfo(ev, mess.id, true);
  });

  $(`.userInRoom tbody #${mess.id} .user-list-title .showUserInfo`).on('click', ev => {
    ev.preventDefault();
    handleShowUserInfo(ev, mess.id, false);
  });

  $(`.userInRoom tbody #${mess.id} .showChatBox`).on('click', ev => {
    ev.preventDefault();
    handleShowChatBox(ev, mess.id);
  });
}

const handleUserLeave = (mess: any) => {
  // remove from user list
  // remove game piece object

  $(`#${mess.id}`).remove();
  state.setGamePiece(mess.id, []);

  state.getGameplay().removePiece(mess.id);
  state.setListUserInRoom(state.getListUserInRoom().filter(x => x.id != mess.id));
  state.setVictoryState(
      state.getVictoryState().filter(x => x.userId != mess.id)
  );

  // let victoryState = state.getVictoryState();
  // victoryState.forEach(x => {
  //   if (x.userId == mess.id) {
  //     x.isQuit = true;
  //   }
  // });
}

const gameObjectIntersectCallback = (gameObj: THREE.Object3D) => {
  switch (gameObj["objInfo"].tag) {
    case 'piece': {
      if (state.getCurrentTurn() == state.getUserId() && gameObj["objInfo"].userId == state.getUserId() 
        && state.getCanMovePieceStatus()) {
        const piece = state.getGamePiece(gameObj["objInfo"].userId)
          .find(x => x.order == gameObj["objInfo"].order);

        if (piece.checkAvailable(state.getPointDice1())) {
          piece.colorAvailable();
        } else {
          piece.colorUnAvailable();
        }
      }
    }
  }
}


const gameObjectIntersectLeaveCallback = (gameObj: THREE.Object3D) => {
  switch (gameObj["objInfo"].tag) {
    case 'piece': {
      if (state.getCurrentTurn() == state.getUserId()) {
        const piece = state.getGamePiece(gameObj["objInfo"].userId)
          .find(x => x.order == gameObj["objInfo"].order);

        piece.colorDefault();
      }
    }
  }
}


const gameObjectIntersectMouseClickCallback = (gameObj: THREE.Object3D) => {
  switch (gameObj["objInfo"].tag) {
    case 'piece': {
      if (state.getCurrentTurn() == state.getUserId() && gameObj["objInfo"].userId == state.getUserId() 
        && state.getCanMovePieceStatus()) {
        const piece = state.getGamePiece(state.getUserId())
          .find(x => x.order == gameObj["objInfo"].order);

        if (piece.checkAvailable(state.getPointDice1())) {

          // if (piece.hack_maxJump == true) {
          //   state.setPointDice1(6);
          // }

          syncPiece(piece);
          state.getGamePiece(state.getUserId()).forEach(x => x.colorDefault());
        }
      }
    }
  }
}

const gameObjectIntersectMouseDownCallback = (gameObj: THREE.Object3D) => {
  // switch (gameObj["objInfo"].tag) {
  //   case 'piece': {
  //     if (state.getCurrentTurn() == state.getUserId() && state.getCanMovePieceStatus()) {
  //       const piece = state.getGamePiece(gameObj["objInfo"].userId)
  //         .find(x => x.order == gameObj["objInfo"].order);


  //       if (piece.checkAvailable(state.getPointDice1() + state.getPointDice2())) {
  //         syncPiece(piece);
  //       }
  //     }
  //   }
  // }
}

// do something revert mouse down effect
const gameObjectIntersectMouseUpCallback = (gameObj: THREE.Object3D) => {
  // switch (gameObj["objInfo"].tag) {
  //   case 'piece': {
  //     const piece = state.getGamePiece(gameObj["objInfo"].userId)
  //       .find(x => x.order == gameObj["objInfo"].order);
  //     piece.makeAvailableColor();
  //   }
  // }
}

const _dbg = (params) => {
  console.log(params);
}

const initGameEvent = () => {
  const gameRoom = state.getGameRoom();


  gameRoom.onLeave((_) => {
    // console.log(`=== Client ${state.getUserId()} leaved ===`)

    $(".leader_board_region .leader_board .btn_region button").on("click", (ev) => {
      state.setLeaderboardInterval(0, null);
      // eslint-disable-next-line no-restricted-globals
      location.reload();
    });

    const INTERVAL_SECS = 15;
    let displayLeaderboard = true;
    state.setLeaderboardInterval(0, null);
    state.setLeaderboardInterval(INTERVAL_SECS, (counter, state) => {
      if (state == 0) {
        if (displayLeaderboard == true) {
          toggleLeaderboard();
          displayLeaderboard = false;
        }
        const padNum = `${INTERVAL_SECS - counter}`.padStart(2, '0');
        $('#leaderBoardCountDown').text(`Quit after (${padNum})`);
      } else {
        location.reload();
      }
    });
  });

  gameRoom.onMessage(UserEndGame, (mess) => {
    state.setEndgameVote(mess.endGameVote);

    $('#endGame').text(`End Game (${state.getEndgameVote()})`);
  });

  gameRoom.onError((code) => {
    location.reload();
  });

  gameRoom.onMessage(UserJoin, (mess) => {
    state.setListUserInRoom(mess.userList);
    // _dbg(state.getListUserInRoom());
    chatState.setListUserInRoom(mess.userList);

    showChatBox();
    $('.userInRoom tbody').empty();

    for (const user of mess.userList) {
      handleAddUserUI(user);
      setUserStatus(user);
    }
  });


  gameRoom.onMessage(StartGame, (mess) => {
    loadGame();

    state.setGameStarted(true);
    state.setHaveThrowDiceStatus(false);
    state.setSkipTurnStatus(true);

    $('#endGame').prop('disabled', false);
    $('#leaveRoom').prop('disabled', false);
    state.setLeaderboardInterval(0, null);

    configToolBoxOnState();
  });

  gameRoom.onMessage(UserLeave, (mess) => {
    _dbg(mess)
    handleUserLeave(mess.removedUser);
  });
  // Init game world entry point
  // MeJoin, GetUserReady = setup camera angle + get current ready user in room
  gameRoom.onMessage(MeJoin, (mess) => {
    state.getGameplay().initGameplay(mess.cameraPos)
      .then(_ => {
        state.getGameRoom().send(GetUserReady, '');
      })
  });
  gameRoom.onMessage(GetUserReady, (mess: any) => {
    // downloadOutput(mess, 'test.json');

    for (const payload of mess.data) {
      handleUserReady(payload);
    }
  });
  gameRoom.onMessage(UserReady, (mess) => {
    if (mess.user.id === state.getUserId()) {
      $('#startGame').prop('disabled', true);
    }
    setUserStatus(mess.user);
    handleUserReady(mess);
  });
  // gameRoom.onMessage(UpdatePieceState, (mess) => {
  //   handleUpdatePiece(mess);
  // });


  gameRoom.onMessage(StartTurn, (mess) => {
    gameRoom.send("_DBG", {
      "clientID": state.getListUserInRoom().find(x => x.id == state.getUserId()).clientId,
      "data": (mess)
    });
    // _dbg((mess.prevUser, state.getCurrentTurn()));
    if (!state.getGameStarted()) {
      return;
    }
    state.setCurrentTurn(mess.userId);
    state.setHaveThrowDiceStatus(false);
    state.setSkipTurnStatus(true);

    setUserTurnIcon(mess.userId);
    configToolBoxOnState();

    if (state.getCurrentTurn() == state.getUserId()) {
      const INTERVAL_SECS = 20;
      const padNum = `${INTERVAL_SECS}`;
      $('#skipTurn').text(`Skip (${padNum})`);
      state.setSkipInterval(0, null);
      state.setSkipInterval(INTERVAL_SECS, (counter, state) => {
        if (state == 0) {
          const padNum = `${INTERVAL_SECS - counter}`.padStart(2, '0');
          $('#skipTurn').text(`Skip (${padNum})`);
        } else {
          $('#skipTurn').trigger("click");
        }
      });
    } else {
      state.setSkipInterval(0, null);
    }
  });


  gameRoom.onMessage(ThrowDice, (mess) => {
    diceManager.throwDice(mess.diceVals, mess.dice.angularVeloc, mess.dice.rotation, () => {
      state.setPointDice1(mess.diceVals[0]);
      state.setSkipTurnStatus(true);
      // state.setPointDice2(mess.diceVals[1]);

      configToolBoxOnState();
    });
  });
  // gameRoom.onMessage(RollDicePoint, (mess) => {
  //   state.setPointDice1(mess.dice1);
  //   state.setPointDice2(mess.dice2);

  //   displayToolBoxOnState();
  // });
  gameRoom.onMessage(SyncPieceState, (mess) => {
    const piece = state.getGamePiece(mess.userId).find(x => x.order === mess.order);

    piece.goByStep(mess.step);
  });

  gameRoom.onMessage(SyncPieceStatePosition, (mess) => {
    const piece = state.getGamePiece(mess.userId).find(x => x.order == mess.pieceOrder);
    piece.jumpToIdx(mess.targetPoint, mess.targetIdx, mess.targetType);
  });
}

const initGamePlay = () => {
  $('.userInRoom tbody').empty();
  $('.selectRoom').hide();
  $('.userInRoom').show();
  $('.gameplay').show();

  initGameEvent()

  state.getGameplay().setGameObjectIntersectCallback(gameObjectIntersectCallback);
  state.getGameplay().setGameObjectIntersectLeaveCallback(gameObjectIntersectLeaveCallback);
  state.getGameplay().setGameObjectIntersectMouseDownCallback(gameObjectIntersectMouseDownCallback);
  state.getGameplay().setGameObjectIntersectMouseUpCallback(gameObjectIntersectMouseUpCallback);
  state.getGameplay().setGameObjectIntersectMouseClickCallback(gameObjectIntersectMouseClickCallback);
}

const leaveRoomHandle = () => {
  state.getGameRoom().send(UserLeave, state.getUserId());
}

const endGameRoomHandle = () => {
  state.getGameRoom().send(UserEndGame, {userId: state.getUserId()});
  $('#endGame').prop('disabled', true);
}

const initUserBtnEvent = () => {
  $('#startGame').on('click', ev => {
    state.getGameRoom().send(UserReady, {userId: state.getUserId()})
  });

  $('#endGame').on('click', ev => {
    endGameRoomHandle();
  });

  $('#leaveRoom').on('click', ev => {
    leaveRoomHandle();
  });

  // handle join room
  $('.selectRoom form').on('submit', ev => {
    ev.preventDefault();

    const formValue = getFormSubmitValue('.selectRoom form');
    joinRoom(state.getListRoom().find(x => x.roomId === formValue['choosenRoomId']));
  });

  // handle signout room
  $('#signoutRoom').on('click', ev => {
    ev.preventDefault();
    url_handle(true);
    // alert("want to signout 2 ?")
  });

  // handle create room
  $('#createRoom').on('click', ev => {
    ev.preventDefault();

    const roomAlias = window.prompt('Input Room Name', 'lets play');

    if (!roomAlias) {
      alert('you have to input roomName');
      return;
    }

    state.getClient().create("gameplay", { roomAlias, userId: state.getUserId() })
        .then(room => {
          state.setCurrentRoomId(room.id);
          state.setGameRoom(room);

          createChatRoom(roomAlias, room.id);

          initGamePlay();
          getRoom();
        })
  });

  // $("#test_btn_01").on("click", ev => {
  //   // window.location.href = "/leaderboard";
  //   toggleLeaderboard();
  // });
}
initUserBtnEvent();

const joinRoom = (room: IRoomClient) => {
  if (!room) {
    alert('room is not available');
    return;
  }
  state.setCurrentRoomId(room.roomId);
  state.getClient().joinById(room.roomId, { userId: state.getUserId() })
    .then(room => {
      state.setGameRoom(room);
      joinChatRoom(room.id);

      initGamePlay();
    })
    .catch(_ => {
    })
}

// handle load room id
const displayRoomId = (arr: IRoomClient[]) => {
  $('.selectRoom form .formBody').empty();

  for (const room of arr.reverse()) {
    const element = $(`
      <div class="form-check">
        <input class="form-check-input" type="radio" name="choosenRoomId" value="${room.roomId}" id="${uuidv4()}" 
          ${arr.findIndex(x => x === room) === 0 ? "checked" : ""}>
        <label class="form-check-label" for="${uuidv4()}">
          ${`${room.roomAlias} (${room.roomId})`}
        </label>
      </div>`);

    $('.selectRoom form .formBody').append(element);
  }
}

const getRoom = (callBack?: any) => {
  state.getClient().getAvailableRooms("gameplay")
    .then((x) => {
      if (x.length > 0) {
        state.setListRoom(x.map(x1 => {
          return {
            roomId: x1.roomId,
            roomAlias: x1.metadata.roomAlias
          }
        }));
        displayRoomId(state.getListRoom());
      } else {
        $('.selectRoom form .formBody').append(
          $(`<div style="margin-top: 10px;">There is currently no room available</div>`),
          $(`<div style="margin-bottom: 50px;">Create new room or refresh your browser</div>`)
        );
      }
      if (callBack) callBack()
    })
    .catch(er => {
      $('.selectRoom form .formBody').append(
        $(`<div style="margin-top: 10px;">Cannot connect to server</div>`),
        $(`<div style="margin-bottom: 50px;">Please try again later</div>`)
      );
    })
}

// PROTOTYPE_PLAYGROUND
require("./prototype/prtt010ir_dicethrow");
require("./prototype/jj4cb_jquery");
